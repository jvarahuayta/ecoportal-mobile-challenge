import 'package:coolmovies/models/movie_details.dart';
import 'package:coolmovies/movie_details_screen/movie_details_controller.dart';
import 'package:coolmovies/remote/fetch_movie_details_by_id_gql.dart';
import 'package:coolmovies/widgets/generic_error.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MovieDetailsFetcher extends StatelessWidget {
  final Widget Function(MovieDetails movieDetails, bool loading) builder;

  const MovieDetailsFetcher({
    Key? key,
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = MovieDetailsController.to;
    return Query(
        options: QueryOptions(
          document: fetchMovieDetailsByIdGql,
          variables: {
            'id': controller.screenArgs.movieId,
            'userId': controller.authController.user.value.id
          },
        ),
        builder: (result, {refetch, fetchMore}) {
          if (result.hasException) {
            return GenericError(onRetry: () async {
              await refetch!();
            });
          }
          final movieDetails = result.isLoading
              ? MovieDetails.empty()
              : MovieDetails.fromJson(result.data?['movieById']);
          return RefreshIndicator(
            onRefresh: () async {
              await refetch!();
            },
            child: Obx(() => builder(
                  controller.myReviewExists
                      ? movieDetails.copyWith(reviews: [
                          controller.myMovieReview.value,
                          ...movieDetails.reviews,
                        ])
                      : movieDetails,
                  result.isLoading,
                )),
          );
        });
  }
}
