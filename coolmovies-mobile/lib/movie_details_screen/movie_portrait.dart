import 'package:coolmovies/movie_details_screen/movie_details_args.dart';
import 'package:coolmovies/widgets/movie_portrait_hero.dart';
import 'package:flutter/material.dart';

class MoviePortrait extends StatelessWidget {
  final MovieDetailsArgs args;
  const MoviePortrait({Key? key, required this.args}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: Colors.blueGrey),
      child: Align(
        alignment: Alignment.center,
        child: SizedBox(
          height: MediaQuery.of(context).size.height / 2.5,
          child: MoviePortraitHero(
            movieId: args.movieId,
            url: args.portraitUrl,
          ),
        ),
      ),
    );
  }
}
