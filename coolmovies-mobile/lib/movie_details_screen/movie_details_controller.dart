import 'package:coolmovies/controllers/auth_controller.dart';
import 'package:coolmovies/models/movie_details.dart';
import 'package:coolmovies/models/movie_review.dart';
import 'package:coolmovies/movie_details_screen/movie_details_args.dart';
import 'package:coolmovies/remote/add_review_gql.dart';
import 'package:coolmovies/remote/fetch_my_movie_review_gql.dart';
import 'package:coolmovies/remote/update_review_gql.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MovieDetailsController extends GetxController {
  final GraphQLClient graphQLClient;
  final AuthController authController;
  final MovieDetailsArgs screenArgs;
  final movieDetails = MovieDetails.empty().obs;
  final myMovieReview = MovieReview.empty().obs;
  final loading = false.obs;

  static MovieDetailsController get to => Get.find();
  bool get myReviewExists => myMovieReview.value.id.isNotEmpty;

  MovieDetailsController({
    required this.graphQLClient,
    required this.authController,
    required this.screenArgs,
  });

  @override
  void onInit() {
    super.onInit();
    loadMyReview();
  }

  void loadMyReview() async {
    final result = await graphQLClient.query(QueryOptions(
        document: fetchMyMovieReviewGql,
        fetchPolicy: FetchPolicy.networkOnly,
        variables: {
          'movieId': screenArgs.movieId,
          'userId': authController.user.value.id
        }));
    if (result.hasException) {
      print(result.exception);
      return;
    }
    // Should be limited to 1
    final reviews =
        (result.data?['allMovieReviews']['nodes'] ?? []) as List<dynamic>;
    if (reviews.length == 1) {
      myMovieReview.value = MovieReview.fromJson(reviews[0]);
    }
  }

  Future<void> saveReview({
    required String title,
    required String body,
    required int rating,
  }) async {
    if (myMovieReview.value.id.isEmpty) {
      final result = await graphQLClient.mutate(
        MutationOptions(
          document: addReviewGql,
          variables: {
            'input': {
              'movieReview': {
                'title': title,
                'body': body,
                'rating': rating,
                'movieId': screenArgs.movieId,
                'userReviewerId': authController.user.value.id
              }
            }
          },
        ),
      );
      if (result.hasException) {
        print(result.exception);
        throw 'save_review.error';
      }
      myMovieReview.value = MovieReview.fromJson(
          result.data?['createMovieReview']['movieReview']);
      Get.back();
      Get.snackbar('Review Added!', '');
    } else {
      final result = await graphQLClient.mutate(
        MutationOptions(
          document: updateReviewGql,
          variables: {
            'reviewId': myMovieReview.value.id,
            'payload': {'title': title, 'body': body, 'rating': rating}
          },
        ),
      );
      if (result.hasException) {
        print(result.exception);
        throw 'save_review.error';
      }
      myMovieReview.value = myMovieReview.value.copyWith(
        title: title,
        body: body,
        rating: rating,
      );
      Get.back();
      Get.snackbar('Review updated!', '');
    }
  }
}
