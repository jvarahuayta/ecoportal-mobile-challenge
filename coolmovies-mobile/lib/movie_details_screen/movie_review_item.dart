import 'package:coolmovies/widgets/rating.dart';
import 'package:flutter/material.dart';
import 'package:coolmovies/models/movie_review.dart';

class MovieReviewItem extends StatelessWidget {
  final MovieReview review;
  final bool isMyReview;

  const MovieReviewItem({
    Key? key,
    required this.review,
    this.isMyReview = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
        style: TextStyle(color: isMyReview ? Colors.blue : Colors.black),
        child: Container(
          padding: EdgeInsets.all(isMyReview ? 10 : 0),
          decoration: BoxDecoration(
            borderRadius: isMyReview ? BorderRadius.circular(10) : null,
            border: isMyReview
                ? Border.all(
                    color: Colors.blue,
                  )
                : null,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    review.title,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Rating(rating: review.rating)
                ],
              ),
              Text(
                'By: ${isMyReview ? 'Me' : review.userName}',
                style: TextStyle(
                    fontSize: 12,
                    fontStyle: FontStyle.italic,
                    color: isMyReview ? Colors.blueAccent : Colors.grey),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                review.body,
              )
            ],
          ),
        ));
  }
}
