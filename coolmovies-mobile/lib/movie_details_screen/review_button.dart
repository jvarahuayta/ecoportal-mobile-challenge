import 'package:coolmovies/movie_details_screen/movie_details_controller.dart';
import 'package:coolmovies/review_form_bottom_sheet/review_form_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReviewButton extends StatelessWidget {
  final bool isEdit;
  const ReviewButton({Key? key, required this.isEdit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = MovieDetailsController.to;
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
      onPressed: () {
        Get.bottomSheet(
            ReviewFormBottomSheet(
              onSave: controller.saveReview,
              title: controller.myMovieReview.value.title,
              body: controller.myMovieReview.value.body,
              rating: controller.myReviewExists
                  ? controller.myMovieReview.value.rating
                  : 3,
            ),
            isDismissible: false,
            enableDrag: false,
            isScrollControlled: true);
      },
      icon: Icon(isEdit ? Icons.edit : Icons.message),
      label: Text(isEdit ? 'Edit review' : 'Add your review'),
    );
  }
}
