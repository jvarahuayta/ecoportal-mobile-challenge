import 'package:coolmovies/models/movie_details.dart';
import 'package:flutter/material.dart';

class MovieInformation extends StatelessWidget {
  final MovieDetails movieDetails;
  const MovieInformation({Key? key, required this.movieDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            const Text(
              'Release date: ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('${movieDetails.releaseDate}')
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            const Text(
              'Director: ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
                '${movieDetails.directorName}, Age: ${movieDetails.directorAge}')
          ],
        )
      ],
    );
  }
}
