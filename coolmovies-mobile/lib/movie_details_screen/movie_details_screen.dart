import 'package:coolmovies/controllers/auth_controller.dart';
import 'package:coolmovies/movie_details_screen/movie_details_args.dart';
import 'package:coolmovies/movie_details_screen/movie_details_controller.dart';
import 'package:coolmovies/movie_details_screen/movie_details_fetcher.dart';
import 'package:coolmovies/movie_details_screen/movie_information.dart';
import 'package:coolmovies/movie_details_screen/movie_portrait.dart';
import 'package:coolmovies/movie_details_screen/movie_review_item.dart';
import 'package:coolmovies/movie_details_screen/review_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MovieDetailsScreen extends StatelessWidget {
  final MovieDetailsArgs args;

  const MovieDetailsScreen({Key? key, required this.args}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      floatingActionButton: Obx(() {
        if (MovieDetailsController.to.loading.value) {
          return Container();
        }
        return ReviewButton(
          isEdit: MovieDetailsController.to.myMovieReview.value.id.isNotEmpty,
        );
      }),
      body: MovieDetailsFetcher(
        builder: (movieDetails, loading) {
          var itemsCount = loading ? 2 : movieDetails.reviews.length + 1;
          return ListView.separated(
              itemCount: itemsCount,
              padding: const EdgeInsets.only(bottom: 50),
              separatorBuilder: (_, index) {
                if (index < 2) {
                  return Container();
                }
                return const Divider(
                  height: 10,
                  thickness: 1,
                  indent: 25,
                  endIndent: 25,
                );
              },
              itemBuilder: (_, index) {
                if (index == 0) {
                  return MoviePortrait(
                    args: args,
                  );
                } else if (loading) {
                  return const CircularProgressIndicator();
                } else if (index == 1) {
                  return Padding(
                    padding: const EdgeInsets.all(20),
                    child: MovieInformation(movieDetails: movieDetails),
                  );
                }
                final movieReview = movieDetails.reviews[index - 2];
                final isMyReview =
                    movieReview.userId == AuthController.to.user.value.id;
                return Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: isMyReview ? 10 : 20,
                    vertical: 10,
                  ),
                  child: MovieReviewItem(
                    review: movieReview,
                    isMyReview: isMyReview,
                  ),
                );
              });
        },
      ),
    );
  }
}
