class MovieDetailsArgs {
  final String movieId;
  final String title;
  final String portraitUrl;
  MovieDetailsArgs(
      {required this.movieId, required this.title, required this.portraitUrl});
}
