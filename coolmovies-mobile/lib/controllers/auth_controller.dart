import 'package:coolmovies/models/user.dart';
import 'package:coolmovies/remote/login_gql.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class AuthController extends GetxController {
  final user = User(id: '').obs;
  static AuthController get to => Get.find();
  bool get logged => user.value.id.isNotEmpty;

  final GraphQLClient graphQLClient;

  AuthController({required this.graphQLClient});

  Future<void> populate() async {
    if (logged) {
      return;
    }
    final result = await graphQLClient.mutate(
        MutationOptions(document: loginGql, variables: {'userName': 'Demo'}));
    if (result.hasException) {
      throw 'auth.populate.error';
    }
    user.value = User.fromJson(result.data?['createUser']['user']);
  }
}
