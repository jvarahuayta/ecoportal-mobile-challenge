import 'package:graphql_flutter/graphql_flutter.dart';

final fetchMoviesGql = gql('''
  query fetchMovies{
    allMovies {
      nodes {
        id
        title
        imgUrl
      }
    }
  }
''');
