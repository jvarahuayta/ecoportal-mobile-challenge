import 'package:graphql_flutter/graphql_flutter.dart';

final addReviewGql = gql(r'''
  mutation addReview ($input: CreateMovieReviewInput!) {
    createMovieReview(input: $input) {
      movieReview {
        id
        title
        rating
        body
        userByUserReviewerId {
          name
          id
        }
      }
    }
  }
''');
