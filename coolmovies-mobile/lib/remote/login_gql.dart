import 'package:graphql_flutter/graphql_flutter.dart';

final loginGql = gql(r'''
  mutation login($userName: String!){
    createUser(input: {user: {name: $userName}}) {
      user {
        id
        name
      }
    }
  }
''');
