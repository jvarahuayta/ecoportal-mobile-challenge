import 'package:graphql_flutter/graphql_flutter.dart';

final updateReviewGql = gql(r'''
  mutation ($reviewId: UUID!, $payload: MovieReviewPatch!){
    updateMovieReviewById(
      input: {
        id: $reviewId
        movieReviewPatch: $payload
      }
    ) {
      movieReview {
        id
      }
    }
  }
''');
