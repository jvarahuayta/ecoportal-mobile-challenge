import 'package:graphql_flutter/graphql_flutter.dart';

final fetchMyMovieReviewGql = gql(r'''
  query myMovieReview($movieId: UUID, $userId: UUID) {
    allMovieReviews(
      filter: {
        movieId: { equalTo: $movieId },
        userReviewerId: { equalTo: $userId }
      }
    ) {
      nodes {
        id
        title
        rating
        body
        userByUserReviewerId {
          name
          id
        }
      }
    }
  }
''');
