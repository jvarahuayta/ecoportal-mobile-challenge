import 'package:graphql_flutter/graphql_flutter.dart';

final fetchMovieDetailsByIdGql = gql(r'''
  query movieById($id: UUID!, $userId: UUID) {
    movieById(id: $id){
      id
      title
      imgUrl
      releaseDate
      movieDirectorByMovieDirectorId {
        name
        age
      }
      movieReviewsByMovieId (
        filter: {
          userReviewerId: {
            notEqualTo: $userId
          }
        }
      ) {
        nodes {
          id
          title
          rating
          body
          userByUserReviewerId {
            name
            id
          }
        }
      }
    }
  }
''');
