import 'package:coolmovies/splash_screen/splash_controller.dart';
import 'package:coolmovies/widgets/generic_error.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Splash screen'),
            const SizedBox(
              height: 30,
            ),
            Obx(() => !SplashController.to.loading.value
                ? SplashController.to.error.value.isNotEmpty
                    ? GenericError(
                        onRetry: SplashController.to.populateUserAndInit)
                    : Container()
                : const CircularProgressIndicator())
          ],
        ),
      ),
    );
  }
}
