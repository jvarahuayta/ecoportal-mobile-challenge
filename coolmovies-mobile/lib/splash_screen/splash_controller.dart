import 'package:coolmovies/constants/routes.dart';
import 'package:coolmovies/controllers/auth_controller.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  final loading = true.obs;
  final error = ''.obs;
  final AuthController authController;
  SplashController({required this.authController});

  static SplashController get to => Get.find();

  @override
  void onInit() {
    super.onInit();
    populateUserAndInit();
  }

  populateUserAndInit() async {
    try {
      await authController.populate();
      Get.offNamed(homeRoute);
    } catch (ex) {
      print(ex);
      error.value = 'Error populating user...';
    } finally {
      loading.value = false;
    }
  }
}
