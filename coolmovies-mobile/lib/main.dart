import 'dart:io';

import 'package:coolmovies/constants/routes.dart';
import 'package:coolmovies/controllers/auth_controller.dart';
import 'package:coolmovies/movies_list_screen/index.dart';
import 'package:coolmovies/movie_details_screen/index.dart';
import 'package:coolmovies/splash_screen/index.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

void main() async {
  // We're using HiveStore for persistence,
  // so we need to initialize Hive.
  await initHiveForFlutter();

  final HttpLink httpLink = HttpLink(
    Platform.isAndroid
        ? 'http://10.0.2.2:5001/graphql'
        : 'http://localhost:5001/graphql',
  );

  final AuthLink authLink = AuthLink(
    getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
  );

  final Link link = authLink.concat(httpLink);

  final client = GraphQLClient(
    link: link,
    cache: GraphQLCache(store: HiveStore()),
  );

  Get.put<GraphQLClient>(client);

  runApp(GraphQLProvider(
    client: ValueNotifier(client),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialBinding: BindingsBuilder(() {
        Get.put(AuthController(graphQLClient: Get.find<GraphQLClient>()));
      }),
      getPages: [
        GetPage(
            name: splashRoute,
            page: () => const SplashScreen(),
            binding: BindingsBuilder(() {
              Get.lazyPut<SplashController>(() =>
                  SplashController(authController: Get.find<AuthController>()));
            })),
        GetPage(
          name: homeRoute,
          page: () => const MoviesListScreen(),
        ),
        GetPage(
          name: detailsRoute,
          page: () => MovieDetailsScreen(args: Get.arguments),
          binding: BindingsBuilder(() {
            Get.put(MovieDetailsController(
              graphQLClient: Get.find(),
              authController: Get.find(),
              screenArgs: Get.arguments,
            ));
          }),
        ),
      ],
    );
  }
}
