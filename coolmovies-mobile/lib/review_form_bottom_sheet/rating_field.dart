import 'package:coolmovies/widgets/rating_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class RatingField extends StatelessWidget {
  final int defaultValue;
  const RatingField({Key? key, required this.defaultValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderField(
      name: 'rating',
      initialValue: defaultValue,
      builder: (FormFieldState<int?> field) {
        return RatingInput(
            value: field.value ?? 1,
            onChange: (val) {
              field.didChange(val);
            });
      },
    );
  }
}
