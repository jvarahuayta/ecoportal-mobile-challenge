import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final bool isEdit;
  const Header({Key? key, required this.isEdit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        isEdit ? 'Edit your review' : 'Write your review',
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 22,
        ),
      ),
    );
  }
}
