import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class BodyField extends StatelessWidget {
  final String defaultValue;
  const BodyField({Key? key, required this.defaultValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: 'body',
      maxLines: 5,
      initialValue: defaultValue,
      validator: FormBuilderValidators.compose(
          [FormBuilderValidators.required(context)]),
      decoration: const InputDecoration(
          border: OutlineInputBorder(borderSide: BorderSide())),
    );
  }
}
