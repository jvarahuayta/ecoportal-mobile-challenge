import 'package:coolmovies/review_form_bottom_sheet/form_actions.dart';
import 'package:coolmovies/review_form_bottom_sheet/body_field.dart';
import 'package:coolmovies/review_form_bottom_sheet/header.dart';
import 'package:coolmovies/review_form_bottom_sheet/rating_field.dart';
import 'package:coolmovies/review_form_bottom_sheet/title_field.dart';
import 'package:coolmovies/widgets/confirm_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class ReviewFormBottomSheet extends StatelessWidget {
  final String title;
  final String body;
  final int rating;
  final Future<void> Function({
    required String title,
    required String body,
    required int rating,
  }) onSave;

  const ReviewFormBottomSheet({
    Key? key,
    required this.onSave,
    required this.title,
    required this.body,
    required this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormBuilderState>();
    return Card(
      margin: EdgeInsets.zero,
      child: SizedBox(
        height: 500,
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: FormBuilder(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 20),
                Header(isEdit: title.isNotEmpty),
                const SizedBox(height: 20),
                TitleField(defaultValue: title),
                const SizedBox(height: 20),
                BodyField(defaultValue: body),
                const SizedBox(height: 20),
                RatingField(defaultValue: rating),
                const SizedBox(height: 20),
                FormActions(onCancel: () async {
                  final String title =
                      _formKey.currentState!.fields['title']?.value;
                  final String body =
                      _formKey.currentState!.fields['body']?.value;
                  if ((title.isNotEmpty && title != this.title) ||
                      (body.isNotEmpty && body != this.body)) {
                    final confirm =
                        await Get.dialog<bool>(const ConfirmDialog()) ?? false;
                    if (!confirm) {
                      return;
                    }
                  }
                  Get.back();
                }, onSave: () {
                  if (_formKey.currentState!.validate()) {
                    onSave(
                      title: _formKey.currentState!.fields['title']?.value,
                      body: _formKey.currentState!.fields['body']?.value,
                      rating: _formKey.currentState!.fields['rating']?.value,
                    );
                  }
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
