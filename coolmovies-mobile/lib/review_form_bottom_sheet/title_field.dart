import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class TitleField extends StatelessWidget {
  final String defaultValue;

  const TitleField({Key? key, required this.defaultValue}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: 'title',
      initialValue: defaultValue,
      autofocus: true,
      validator: FormBuilderValidators.compose(
          [FormBuilderValidators.required(context)]),
      decoration: const InputDecoration(
          border: OutlineInputBorder(borderSide: BorderSide())),
    );
  }
}
