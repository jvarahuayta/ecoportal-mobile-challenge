import 'package:flutter/material.dart';

class FormActions extends StatelessWidget {
  final VoidCallback onSave;
  final VoidCallback onCancel;

  const FormActions({Key? key, required this.onSave, required this.onCancel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        TextButton(onPressed: onCancel, child: const Text('Cancel')),
        ElevatedButton(
            style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
            onPressed: onSave,
            child: const Text('Send'))
      ],
    );
  }
}
