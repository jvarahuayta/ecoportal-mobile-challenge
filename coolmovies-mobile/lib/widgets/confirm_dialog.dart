import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmDialog extends StatelessWidget {
  const ConfirmDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: const Text('Leave changes?'),
      actions: <Widget>[
        TextButton(
          child: const Text('Cancel'),
          onPressed: () {
            Get.back(result: false);
          },
        ),
        ElevatedButton(
          child: const Text('Confirm'),
          onPressed: () {
            Get.back(result: true);
          },
        ),
      ],
    );
  }
}
