import 'package:flutter/material.dart';

class Rating extends StatelessWidget {
  final int rating;
  const Rating({Key? key, required this.rating}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.star,
          color: Colors.blue,
        ),
        Text(rating.toString())
      ],
    );
  }
}
