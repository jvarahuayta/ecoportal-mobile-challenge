import 'package:flutter/material.dart';

class MoviePortraitHero extends StatelessWidget {
  final String movieId;
  final String url;
  const MoviePortraitHero({Key? key, required this.url, required this.movieId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'movie-portrait-$movieId',
      child: Image.network(
        url,
        alignment: Alignment.topCenter,
        fit: BoxFit.cover,
      ),
    );
  }
}
