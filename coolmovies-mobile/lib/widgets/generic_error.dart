import 'package:flutter/material.dart';

class GenericError extends StatelessWidget {
  final VoidCallback onRetry;
  const GenericError({Key? key, required this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.error),
          const SizedBox(
            height: 20,
          ),
          const Text('Error ocurred :/'),
          TextButton(onPressed: onRetry, child: const Text('Retry'))
        ],
      ),
    );
  }
}
