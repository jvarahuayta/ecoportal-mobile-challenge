import 'package:flutter/material.dart';

class RatingInput extends StatelessWidget {
  final int value;
  final void Function(int) onChange;
  final int maxValue;

  const RatingInput({
    Key? key,
    required this.value,
    required this.onChange,
    this.maxValue = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(
        maxValue,
        (index) => IconButton(
          visualDensity: VisualDensity.compact,
          onPressed: () {
            onChange(index + 1);
          },
          icon: Icon(
            index + 1 <= value ? Icons.star : Icons.star_border,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
