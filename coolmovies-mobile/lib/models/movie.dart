class Movie {
  final String id;
  final String title;
  final String portraitUrl;
  Movie({required this.id, required this.title, required this.portraitUrl});
}
