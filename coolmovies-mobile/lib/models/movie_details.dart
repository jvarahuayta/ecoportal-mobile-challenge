import 'package:coolmovies/models/movie_review.dart';

class MovieDetails {
  String id;
  String releaseDate;
  String directorName;
  int directorAge;
  List<MovieReview> reviews;

  MovieDetails._({
    required this.id,
    required this.directorAge,
    required this.directorName,
    required this.releaseDate,
    required this.reviews,
  });

  MovieDetails.empty()
      : id = '',
        releaseDate = '',
        directorAge = 0,
        reviews = [],
        directorName = '';

  MovieDetails.fromJson(dynamic json)
      : id = json['id'],
        releaseDate = json['releaseDate'],
        directorName = json['movieDirectorByMovieDirectorId']['name'],
        directorAge = json['movieDirectorByMovieDirectorId']['age'],
        reviews =
            ((json['movieReviewsByMovieId']['nodes'] ?? []) as List<dynamic>)
                .map((e) => MovieReview.fromJson(e))
                .toList();

  MovieDetails copyWith({List<MovieReview>? reviews}) {
    return MovieDetails._(
      id: id,
      directorAge: directorAge,
      directorName: directorName,
      releaseDate: releaseDate,
      reviews: reviews ?? this.reviews,
    );
  }
}
