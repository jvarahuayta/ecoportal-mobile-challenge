class MovieReview {
  String id;
  String title;
  int rating;
  String body;
  String userName;
  String userId;

  MovieReview({
    this.id = '',
    this.title = '',
    this.rating = 0,
    this.body = '',
    this.userId = '',
    this.userName = '',
  });

  MovieReview.empty()
      : id = '',
        title = '',
        rating = 0,
        body = '',
        userName = '',
        userId = '';

  MovieReview.fromJson(dynamic json)
      : id = json['id'],
        title = json['title'],
        rating = json['rating'],
        body = json['body'],
        userName = json['userByUserReviewerId']['name'],
        userId = json['userByUserReviewerId']['id'];

  MovieReview copyWith({String? title, String? body, int? rating}) {
    return MovieReview(
      id: id,
      userId: userId,
      userName: userName,
      body: body ?? this.body,
      rating: rating ?? this.rating,
      title: title ?? this.title,
    );
  }
}
