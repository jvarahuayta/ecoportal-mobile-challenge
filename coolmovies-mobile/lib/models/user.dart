class User {
  late final String id;
  late final String name;

  User({required this.id});

  User.fromJson(dynamic json)
      : id = json['id'],
        name = json['name'];
}
