import 'package:coolmovies/constants/routes.dart';
import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/movie_details_screen/movie_details_args.dart';
import 'package:coolmovies/widgets/movie_portrait_hero.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MovieItem extends StatelessWidget {
  final Movie movie;

  const MovieItem({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 4,
      child: Stack(
        children: [
          Positioned.fill(
            child: MoviePortraitHero(url: movie.portraitUrl, movieId: movie.id),
          ),
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                Get.toNamed(detailsRoute,
                    arguments: MovieDetailsArgs(
                        movieId: movie.id,
                        title: movie.title,
                        portraitUrl: movie.portraitUrl));
              },
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration:
                      BoxDecoration(color: Colors.black.withOpacity(.6)),
                  child: Text(
                    movie.title,
                    maxLines: 2,
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
