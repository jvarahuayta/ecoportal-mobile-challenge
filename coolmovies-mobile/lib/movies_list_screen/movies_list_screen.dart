import 'package:coolmovies/movies_list_screen/movies_list.dart';
import 'package:coolmovies/movies_list_screen/movies_list_fetcher.dart';
import 'package:flutter/material.dart';

class MoviesListScreen extends StatelessWidget {
  const MoviesListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies list'),
      ),
      body: MoviesListFetcher(
        builder: (movies) => MoviesList(movies: movies),
      ),
    );
  }
}
