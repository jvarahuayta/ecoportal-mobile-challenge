import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/remote/fetch_movies_gql.dart';
import 'package:coolmovies/widgets/generic_error.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MoviesListFetcher extends StatelessWidget {
  final Widget Function(List<Movie> movies) builder;

  const MoviesListFetcher({Key? key, required this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(document: fetchMoviesGql),
      builder: (result, {refetch, fetchMore}) {
        if (result.hasException) {
          return GenericError(onRetry: () async {
            await refetch!();
          });
        }
        final movies = (result.data?['allMovies']['nodes'] as List<dynamic>)
            .map(
              (movieJson) => Movie(
                id: movieJson['id'],
                title: movieJson['title'],
                portraitUrl: movieJson['imgUrl'],
              ),
            )
            .toList();
        return RefreshIndicator(
            child: builder(movies),
            onRefresh: () async {
              await refetch!();
            });
      },
    );
  }
}
